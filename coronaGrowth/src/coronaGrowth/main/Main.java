package coronaGrowth.main;

import java.util.ArrayList;
import java.util.List;

import coronaGrowth.data.Process;
import coronaGrowth.data.Situation;

/**
 * 
 * @author valdemir.junior
 * email: tec.valdemir@gmail.com
 *
 */
public class Main {

	static Process process = new Process();

	static {
		System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
	}

	public static void main(String[] args) {

		try {

			System.out.println("Iniciando processamento.");
			
			String dataInicio = null;
			if(args.length > 0){
				dataInicio = args[0]; // mm-dd-yyyy

				if(!process.validDate(dataInicio)){
					System.out.println("Formato inv�lido (m�s-dia-ano), ser� usado o valor default: 01-22-2020");
					dataInicio = "01-22-2020";
				}
				
			}else{
				dataInicio = "01-22-2020";
			}

			String out = "c:\\";

			List<Situation> data = new ArrayList<Situation>();

			//Processando dados
			System.out.println("Processando dados");
			data = process.readCasesBrazil(dataInicio);

			System.out.println("Escrevendo arquivo");
			process.createExcel(data,out);
			
			System.exit(0);
		} catch (Exception e) {
			System.out.println("Erro na execu��o."+e);
			System.exit(-1);
		}

	}

}
