package coronaGrowth.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Pattern;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/**
 * 
 * @author valdemir.junior
 *
 */
public class Process {

	/**
	 * M�todo que l� os dados do reposit�rio GIT da covid-19
	 * @param dataInicio
	 * @return
	 * @throws Exception
	 */
	public List<Situation> readCasesBrazil(String dataInicio) throws Exception{
		
		List<Situation> retorno = new ArrayList<Situation>();;
		
		try {
			
			List<Situation> situations = readURLs(dataInicio);
			 
			for (Situation situation : situations) {
				
				BufferedReader br;
				try {
					br = new BufferedReader(new InputStreamReader(
							situation.getUrl().openStream()));
				} catch (FileNotFoundException e) {
					 System.out.println("Finalizou Execu��o. N�o existe mais arquivos");
					 break;
				}
				
				String s;
				int cont = 0;
				int line = 0;
				boolean brazil = false;
				String fild = "";

				//Tratamento para Layout do .csv diferente
				boolean latLongLast = false;
				String fildConfirmed = "";
				String fildDeath = "";
				String fildRecovered = "";
				
				System.out.println("Read: "+ situation.getUrl());
				
				situation.setConfirmed(new Integer(0));
				situation.setDeaths(new Integer(0));
				situation.setRecovered(new Integer(0));
				
				
				while ((s = br.readLine()) != null) {

					if (s.indexOf("Brazil") != -1) {
						brazil = true;
						cont += 1;
					}
					
					if (brazil) {
						
						line += 1;

						if (line == 3) {
							fild = s.replace("<td>", "").replace("</td>", "");
							if(fild.trim().equals("")){
								fild = "0";
							}
							fildConfirmed = fild;
						}else if(line == 4){
							fild = s.replace("<td>", "").replace("</td>", "");
							if(fild.trim().equals("")){
								fild = "0";
							}
							fildDeath = fild;
						}else if(line == 5){
							fild = s.replace("<td>", "").replace("</td>", "");
							if(fild.trim().equals("")){
								fild = "0";
							}
							fildRecovered = fild;
						}
						
						
						if (line == 6 && (s.indexOf("-") != -1) || s.trim().indexOf("</tr>") != -1) {
							latLongLast = true;
						}
						
						if(latLongLast && line == 6){
							
							situation.setConfirmed(situation.getConfirmed() + new Integer(fildConfirmed.trim()));
							situation.setDeaths(situation.getDeaths() +  new Integer(fildDeath.trim()));
							situation.setRecovered(situation.getRecovered() + new Integer(fildRecovered.trim()));
						
						}else if (!latLongLast){
						
							if (line == 5) {
								fild = s.replace("<td>", "").replace("</td>", "");
								if(fild.trim().equals("")){
									fild = "0";
								}
								situation.setConfirmed(situation.getConfirmed() + new Integer(fild.trim()));
							}
							if (line == 6) {
								fild = s.replace("<td>", "").replace("</td>", "");
								if(fild.trim().equals("")){
									fild = "0";
								}
								situation.setDeaths(situation.getDeaths() + new Integer(fild.trim()));
							}
							if (line == 7) {
								fild = s.replace("<td>", "").replace("</td>", "");
								if(fild.trim().equals("")){
									fild = "0";
								}
								situation.setRecovered(situation.getRecovered() + new Integer(fild.trim()));
							}
							
						}
						
					}

					if(brazil && s.trim().indexOf("</tr>") != -1 && line == 6){
						cont = 0;
						line = 0;
						brazil = false;
					}
					
					if (cont == 2) {
						cont = 0;
						line = 0;
						brazil = false;
					}
				}
				
				br.close();
				
				retorno.add(situation);
			}
			
	      }catch (MalformedURLException excecao) {
	    	  System.out.println("URL mal formatada.");
	      } catch (Exception excecao) {
	    	  System.out.println("Erro gen�rico. "+excecao);
	      }
		
		return retorno;
	}

	/**
	 * Recuperando as URL's com os dados da COVID-19
	 * @param dataInicio
	 * @return
	 * @throws Exception
	 */
	private List<Situation> readURLs(String dataInicio) throws Exception {

		List<Situation> returnList = new ArrayList<Situation>();
		
		Date date = new SimpleDateFormat("MM-dd-yyyy").parse(dataInicio); 	 
		
		Date today = new Date();
		
		while(date.getTime() <= today.getTime()){
			
			Situation situation = new Situation();
			
			DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
			String strDate = dateFormat.format(date);
			
			URL url = new URL("https://github.com/CSSEGISandData/COVID-19/blob/master/csse_covid_19_data/csse_covid_19_daily_reports/"+strDate+".csv");
			
			situation.setDate(strDate);
			situation.setUrl(url);
			
			returnList.add(situation);
			
			//plus one day
			date = new Date(date.getTime() + (1000 * 60 * 60 * 24));
		}
		
		
		return returnList;
	}

	public void createExcel(List<Situation> data, String out) throws IOException, RowsExceededException, 
	WriteException, BiffException {
		
    	WritableFont bold = new WritableFont(WritableFont.ARIAL,                 
    	10, WritableFont.BOLD);         
    	bold.setColour(Colour.WHITE);         
    	WritableCellFormat arial10font = new WritableCellFormat(bold);         
    	arial10font.setBackground(Colour.BLUE);         
    	arial10font.setAlignment(Alignment.CENTRE);         
    	arial10font.setBorder(Border.ALL,BorderLineStyle.MEDIUM,Colour.BLACK); 

    	
    	GregorianCalendar calendar = new GregorianCalendar();   
        SimpleDateFormat formatador = new SimpleDateFormat("dd-MM-yyyy");     
           
        String filename = out+"coronaVirus_Brazil_"+formatador.format(calendar.getTime())+".xls"; 
    	
        WritableWorkbook workbook = Workbook.createWorkbook(new File(filename));   
           
        WritableSheet sheet = workbook.createSheet("Dados", 0);
        
        
    	//Cabe�alho
        Label labelTitulo0 = new Label(0, 0, "Data", arial10font);   
        sheet.addCell(labelTitulo0);
        
        Label labelTitulo1 = new Label(1, 0, "Confirmados", arial10font);   
        sheet.addCell(labelTitulo1);
        
        Label labelTitulo2 = new Label(2, 0, "Mortes", arial10font);   
        sheet.addCell(labelTitulo2);
        
        Label labelTitulo3 = new Label(3, 0, "Recuperados", arial10font);   
        sheet.addCell(labelTitulo3);
        
        //Dados
        int i = 1;

		for (Situation situation : data) {

			if(situation.getConfirmed().intValue() > 0 
					|| situation.getDeaths().intValue() > 0 
					|| situation.getRecovered() > 0){
				
				Label label = new Label(0, i, situation.getDate());
				sheet.addCell(label);
	
				jxl.write.Number confirmed = new jxl.write.Number(1, i, situation.getConfirmed()); 
				sheet.addCell(confirmed);
	
				jxl.write.Number deaths = new jxl.write.Number(2, i, situation.getDeaths()); 
				sheet.addCell(deaths);
	
				jxl.write.Number recovered = new jxl.write.Number(3, i, situation.getRecovered()); 
				sheet.addCell(recovered);
				
				i++;
				
			}
		}
        
        // Escrevedo o arquivo em disco   
        workbook.write();   
           
        // Fechando a IO   
        workbook.close();   
    	
        System.out.println("Arquivo gerado com sucesso!");
	}
	
	private static Pattern DATE_PATTERN = Pattern.compile("^((0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01])-((19|2[0-9])[0-9]{2}))$");
		 
	public boolean validDate(String date) {
		return DATE_PATTERN.matcher(date).matches();
	}

}
