package coronaGrowth.data;

import java.net.URL;

/**
 * 
 * @author valdemir.junior
 *
 */
public class Situation {

	private String date;
	private Integer confirmed;
	private Integer deaths;
	private Integer recovered;
	private URL url;
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getConfirmed() {
		return confirmed;
	}
	public void setConfirmed(Integer confirmed) {
		this.confirmed = confirmed;
	}
	public Integer getDeaths() {
		return deaths;
	}
	public void setDeaths(Integer deaths) {
		this.deaths = deaths;
	}
	public Integer getRecovered() {
		return recovered;
	}
	public void setRecovered(Integer recovered) {
		this.recovered = recovered;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "Situation [date=" + date + ", confirmed=" + confirmed
				+ ", deaths=" + deaths + ", recovered=" + recovered + ", url="
				+ url + "]";
	}

}
